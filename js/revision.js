// THREE js revision
if (THREE.REVISION > 71) {
	THREE.ParticleBasicMaterial = THREE.PointsMaterial;
	THREE.ParticleSystem = THREE.Points;
}
else 
if (THREE.REVISION > 67) {
	THREE.ParticleBasicMaterial = THREE.PointCloudMaterial;
	THREE.ParticleSystem = THREE.PointCloud;
}
if ((typeof _typeface_js !== 'object') && (typeof THREE.FontLoader === 'function')) {
	_typeface_js = new THREE.FontLoader();
	_typeface_js.loadFace = function(json) { this._font = _typeface_js.parse(json); };
	THREE.FontUtils = {
		generateShapes:  function(f,p) { return _typeface_js._font.generateShapes(f,p.size); } 
	}; 
} 
if (typeof WeakMap !== 'function') {
	var WeakMap = function() { this.clear(); return this; };
	WeakMap.prototype.delete = function(key) {
		for (var i in this.key) {
			if (this.key[i] === key) {
				this.key.splice(i,1);
				this.value.splice(i,1);
				break;
			}		
		}
		return;
	};
	WeakMap.prototype.clear = function() {
		this.value = [];
		this.key = [];
	};
	WeakMap.prototype.get = function(key) {
		for (var i in this.key) {
			if (this.key[i] === key) {
				return this.value[i];
			}		
		}
		return undefined;
	};
        WeakMap.prototype.has = function(key) {
		for (var i in this.key) {
			if (this.key[i] === key) {
				return true;
			}		
		}
		return false;
	};
	WeakMap.prototype.set = function(key,value) {
		this.value.push(value);
		this.key.push(key);
	};
}
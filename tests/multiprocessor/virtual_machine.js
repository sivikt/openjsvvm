var config = {
  state: {
    volume: 655360
  },
  core: {
    quant: 1,
    processor: [
      {
        state: {
          volume: 64,
          pointer: 1000
        },
        core: {
          implementation: {
            "0000": "",
            "0001": "var o = memory[(registers[0]+=2)-1];\nif (o > 2147483647) o -= 4294967296;\nregisters[0] += o;/*!*/",
            "0002": "var c = (registers[0] += 3); var start = registers[memory[c - 1]]; var pointer = registers[memory[c - 2]]; \nmachine.stop();\nmachine.addProcessor(processor,processor.newRegisters(start, pointer),pointer);\nmachine.step()",
            "0003": "registers[0] += 1; machine.stop(); var index = machine.processor.indexOf(this); machine.processor.splice(index, 1); machine.step();",
            "0004": "var c =(registers[0]+=3); registers[memory[c-1]] = memory[c-2];",
          },
          interface: {
            "#nop": "0000",
            "#jump": "0001",
            "#call_execute": "0002",
            "#exit": "0003",
            "#load_value": "0004",
          },
          constructors: {
          }
        }
      }
    ]
  }
}

var defaultCallback = function() {
  console.log("Machine is working");
}

function createVirtualMachine(program, callback) {
  console.log(program)
  config.state.memory = program;
  config.callback = callback?callback:defaultCallback;
  return new VirtualMachine(config);
}